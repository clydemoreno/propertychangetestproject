﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;

namespace ProperyChangeProject
{
    public class ClassLibrary : INotifyPropertyChanged
    {
        private int price;
        public event PropertyChangedEventHandler PropertyChanged;
        public int Price
        {
            get { return price; }
            set { 
                price = value;
                OnPropertyChanged(price.ToString());
                
            }
        }
        private void OnPropertyChanged(string s)
        {
            PropertyChangedEventHandler handler = PropertyChanged;
            if (handler != null)
            {
                handler(this, new PropertyChangedEventArgs(s));
            }
        }
        public void OnCompleted()
        {
            price = 60;
        }
        
    }
}
